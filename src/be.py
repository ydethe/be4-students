import numpy as np
from numpy.fft import fft, fftshift, fftfreq
from numpy import sqrt, log10, exp, pi
from matplotlib import pyplot as plt


# %%
# Define parameters that describe the scenario and the processing
c = 3e8
Tr = 1e-3
T = Tr / 20
Ts = T / 100
noise_pow = 1.0
amp = exp(1j * pi / 6) * 5
t_target = 300e-6
d_target = t_target * c / 2
B = 200e3
np.random.seed(461351657)
# %%


def phase_unfold(sig, eps=1e-9):
    n = len(sig)
    pha = np.zeros(n)
    pha[0] = np.angle(sig[0])

    init_ok = False
    i = 0
    while True:
        if np.abs(sig[i]) > eps:
            init_ok = True
            pha[0 : 1 + i] = np.angle(sig[i])
            break

        i += 1
        if i == n:
            break

    if not init_ok or i == n - 1:
        return pha

    for j in range(i + 1, n):
        if np.abs(sig[j - 1]) < eps or np.abs(sig[j]) < eps:
            r = 0
        else:
            r = sig[j] / sig[j - 1]
            r /= np.abs(r)

        # Nyquist–Shannon sampling theorem garantees that |dpha| < pi
        # So we can call np.angle which will not produce any ambiguity
        dpha = np.angle(r)

        pha[j] = pha[j - 1] + dpha

    return pha


def plot_amp_pha(tps, z, title, pdb=True, unfold=True):
    fig = plt.figure()
    fig.suptitle(title)

    pwr = np.abs(z) ** 2
    if pdb:
        pwr = 10 * log10(pwr)
        p_unt = "dB"
    else:
        p_unt = "$|.|^2"

    axe_amp = fig.add_subplot(211)
    axe_amp.grid(True)
    axe_amp.plot(tps * 1e6, pwr)
    axe_amp.set_ylabel(f"Power ({p_unt})")

    if unfold:
        pha = phase_unfold(z)
        txt = " (unfolded)"
    else:
        pha = np.angle(z)
        txt = ""

    axe_pha = fig.add_subplot(212, sharex=axe_amp)
    axe_pha.grid(True)
    axe_pha.plot(tps * 1e6, pha * 180 / pi)
    axe_pha.set_xlabel("Time (µs)")
    axe_pha.set_ylabel(f"Phase{txt} (deg)")

    return axe_amp, axe_pha


def compute_psd(y):
    K = len(y)
    freq = fftshift(fftfreq(K, Ts))
    psd = fftshift(1 / K * np.abs(fft(y)) ** 2)
    return freq, psd


# %%
# Signal generation
def question_04(show=True):
    ns = int(np.ceil(Tr / Ts))
    kT = int(np.ceil(T / Ts))

    tps = np.arange(ns) * Ts
    e = np.zeros(ns, dtype=np.complex128)
    e[:kT] += 1

    if show:
        fig = plt.figure()
        axe = fig.add_subplot(111)
        axe.grid(True)
        axe.plot(tps * 1e6, np.abs(e) ** 2)
        axe.set_xlabel("Time (µs)")
        axe.set_ylabel(r"Power ($ | e^2 | $)")
        axe.set_title(
            "Transmitted Waveform (T=%.0f µs, fs=%.0f kHz)" % (T * 1e6, 1e-3 / Ts)
        )

    return e


def question_05(e, show=True):
    ns = int(np.ceil(Tr / Ts))
    k_tau = int(np.ceil(2 * d_target / c / Ts))
    print("k_tau = %i" % k_tau)

    tps = np.arange(ns) * Ts
    a = amp * np.roll(e, k_tau)

    if show:
        fig = plt.figure()
        axe = fig.add_subplot(111)
        axe.grid(True)
        axe.plot(tps * 1e6, np.abs(a) ** 2)
        axe.set_xlabel("Time (µs)")
        axe.set_ylabel(r"Power ($|e|^2$)")
        axe.set_title("Received Signal (T=%.0f µs, fs=%.0f kHz)" % (T * 1e6, 1e-3 / Ts))

    return a


def question_06(a, show=True):
    ns = len(a)

    tps = np.arange(ns) * Ts
    nr = sqrt(noise_pow / 2) * np.random.normal(size=ns)
    ni = sqrt(noise_pow / 2) * np.random.normal(size=ns)
    noise = nr + 1j * ni
    x = a + noise

    if show:
        fig = plt.figure()
        axe = fig.add_subplot(111)
        axe.grid(True)
        axe.plot(tps * 1e6, np.abs(x) ** 2)
        axe.set_xlabel("Time (µs)")
        axe.set_ylabel(r"Power ($|e|^2$)")
        axe.set_title(
            "Received Noisy Signal (T=%.0f µs, fs=%.0f kHz)" % (T * 1e6, 1e-3 / Ts)
        )

    return x, nr, ni


# %%

# %%
# Noise analysis
def question_07(nr, ni, show=True):
    n = nr + 1j * ni

    print("Mean = %.4f" % np.mean(nr))
    print("Var = %.4f" % np.var(nr))

    K = len(nr)
    kT = int(np.ceil(T / Ts))
    print("kT = %i" % kT)
    print("K = %i" % K)

    nth = np.linspace(-5, 5, 200)
    th = 1 / sqrt(pi * noise_pow) * exp(-(nth**2) / noise_pow)

    freq, psd = compute_psd(n)

    gam = 5.0
    f0 = 1.0 / 4.0
    k = np.arange(K)
    c = gam * exp(2 * pi * 1j * k * f0)
    psdc = fftshift(1 / K * np.abs(fft(n + c)) ** 2)

    if show:
        fig = plt.figure()
        axe = fig.add_subplot(111)
        axe.grid(True)
        axe.hist(nr, density=True, bins=30)
        axe.plot(nth, th, label="Theory")
        axe.set_xlabel("Power")
        axe.set_ylabel("Fraction")
        axe.set_title("Probability Density Function for $n_r$")
        axe.legend()

        fig = plt.figure()
        axe = fig.add_subplot(111)
        axe.grid(True)
        axe.plot(freq / 1000, 10 * log10(psd))
        # axe.plot(freq, 10 * log10(psd2))
        axe.set_xlabel("Frequency (kHz)")
        axe.set_ylabel("Power (dB)")
        axe.set_title("Power Spectral Density of $n$")

        fig = plt.figure()
        axe = fig.add_subplot(111)
        axe.grid(True)
        axe.plot(freq / 1000, 10 * log10(psdc))
        # axe.plot(freq, 10 * log10(psd2))
        axe.set_xlabel("Frequency (kHz)")
        axe.set_ylabel("Power (dB)")
        axe.set_title("Power Spectral Density of $c+n$")

    return x, nr, ni


# %%

# %%
# Signal processing
def question_09(x, e, show=False):
    K = len(x)
    y = np.correlate(x, e, mode="full")
    k = np.arange(-(K - 1), K)

    print("Range resolution : %.1f km" % (c / 2 * T * 1e-3))

    if show:
        fig = plt.figure()
        axe = fig.add_subplot(111)
        axe.grid(True)
        axe.plot(k * Ts * 1e-3 * c / 2, np.abs(y))
        axe.set_xlabel("Distance (km)")
        axe.set_ylabel("Amplitude")
        axe.set_title("Correlated signal")

        fig = plt.figure()
        axe = fig.add_subplot(111)
        axe.grid(True)
        axe.plot(k * Ts * 1e6, np.abs(y))
        axe.set_xlabel("Time (µs)")
        axe.set_ylabel("Amplitude")
        axe.set_title("Correlated signal")

    return y


# %%

# %%
# Chirp processing
def question_13(show=False):
    # Signal generation. Output : x
    # TX
    K = int(np.ceil(Tr / Ts))
    k_tau = int(np.ceil(2 * d_target / c / Ts))
    kT = int(np.ceil(T / Ts))
    tps = np.arange(K) * Ts
    e = np.zeros(K, dtype=np.complex128)
    e[:kT] = exp(2 * pi * 1j * B / T * tps[:kT] ** 2 / 2)

    if show:
        plot_amp_pha(
            tps,
            e,
            title="Transmitted Waveform (T=%.0f µs, B=%.0f kHz, fs=%.0f kHz)"
            % (T * 1e6, B / 1000, 1e-3 / Ts),
            pdb=False,
        )

    # RX (delay + noise)
    a = amp * np.roll(e, k_tau)
    nr = sqrt(noise_pow / 2) * np.random.normal(size=K)
    ni = sqrt(noise_pow / 2) * np.random.normal(size=K)
    noise = nr + 1j * ni
    x = a + noise

    if show:
        plot_amp_pha(
            tps,
            x,
            title="Received Signal (T=%.0f µs, B=%.0f kHz, fs=%.0f kHz)"
            % (T * 1e6, B / 1000, 1e-3 / Ts),
            pdb=True,
        )

    # Matched filtering
    y = np.correlate(x, e, mode="full")
    k = np.arange(-(K - 1), K)
    tps_corr = k * Ts

    if show:
        plot_amp_pha(
            tps_corr,
            y,
            title="Processed Signal (T=%.0f µs, B=%.0f kHz, fs=%.0f kHz)"
            % (T * 1e6, B / 1000, 1e-3 / Ts),
            unfold=False,
            pdb=True,
        )

    # Filtered zoomed
    if show:
        fig = plt.figure()
        fig.suptitle(
            "Processed Signal, zoomed in (T=%.0f µs, B=%.0f kHz, fs=%.0f kHz)"
            % (T * 1e6, B / 1000, 1e-3 / Ts),
        )
        axe = fig.add_subplot(111)
        axe.grid(True)
        yp = np.max(np.abs(y))
        t_th = np.linspace(-1 / B, 1 / B, 200)
        y_th = np.sinc(t_th * B) * yp
        axe.plot(tps_corr * 1e6, 20 * log10(np.abs(y)))
        axe.plot(
            (t_th + t_target) * 1e6, 20 * log10(np.abs(y_th)), label="Theoritical sinc"
        )
        axe.annotate(
            text=f"{t_target*1e6:.1f},{20*log10(yp):.1f}",
            xy=(t_target * 1e6, 20 * log10(yp)),
        )
        axe.annotate(
            text=f"{t_target*1e6-2.21:.1f},{20*log10(yp)-3:.1f}",
            xy=(t_target * 1e6 - 2.21, 20 * log10(yp) - 3),
            xytext=(t_target * 1e6 - 2.21 - 10, 20 * log10(yp) - 3),
        )
        axe.annotate(
            text=f"{t_target*1e6+2.21:.1f},{20*log10(yp)-3:.1f}",
            xy=(t_target * 1e6 + 2.21, 20 * log10(yp) - 3),
        )
        axe.scatter(
            [t_target * 1e6 - 2.21, t_target * 1e6, t_target * 1e6 + 2.21],
            [20 * log10(yp) - 3, 20 * log10(yp), 20 * log10(yp) - 3],
            marker="o",
        )
        axe.set_xlabel("Time (µs)")
        axe.set_ylabel("Power (dB)")
        axe.legend()

    # PSD of x
    freq, psd = compute_psd(x)

    if show:
        fig = plt.figure()
        axe = fig.add_subplot(111)
        axe.grid(True)
        axe.plot(freq / 1000, 10 * log10(psd))
        axe.set_xlabel("Frequency (kHz)")
        axe.set_ylabel("Power (dB)")
        axe.set_title("Power Spectral Density of x")


# %%

if __name__ == "__main__":
    print("fs = %.1f MHz" % (1e-6 / Ts))
    print("B = %.1f kHz" % (1e-3 * B))

    e = question_04(show=True)
    a = question_05(e, show=True)
    x, nr, ni = question_06(a, show=True)
    question_07(nr, ni, show=True)
    y = question_09(x, e, show=True)
    question_13(show=True)

    plt.show()
