import numpy as np
from numpy import mean, sqrt, real, imag, conj, arctan, sin, cos
from scipy.io import loadmat


def readdata_radar2400AD2(file):
    raw = loadmat(file)

    date = raw["DATE"][0]
    wf = raw["WAVEFORM"][0]
    Fc = np.float64(raw["CENTERFREQUENCY"][0, 0])
    Tr = np.float64(raw["SWEEPTIME"][0, 0] * 1e-3)
    L = int(raw["samplenumberpersweep"][0, 0])
    B = np.float64(raw["BANDWIDTH"][0, 0])
    s_mix_1 = conj(raw["DATA1"][0, :])  # data of channel 1
    s_mix_2 = conj(raw["DATA2"][0, :])  # data of channel 2
    M = len(s_mix_1) // L

    return date, wf, Fc, B, Tr, L, M, s_mix_1, s_mix_2


def iq_imbalance_comp(x):
    # IQ_IMBALANCE_COMP IQ imbalance compensation.
    # Y = IQ_IMBALANCE_COMP(X) returns the complex signal Y that corresponds
    # after IQ imbalance compensation.
    #
    # [Y,PHI] = IQ_IMBALANCE_COMP(X) returns also the angle PHI of the IQ
    # imbalance in radians.

    # 2017-07-29|stephanie.bidon@isae-supaero.fr
    # References
    # http://ancortek.com/wp-content/uploads/2015/06/Collecting-Data-using-Ancorteks-SDR-C-API.mp4

    ##
    I = real(x)
    Q = imag(x)
    # --
    m_I = mean(I)
    m_Q = mean(Q)
    v_I = mean((I - m_I) ** 2)
    v_Q = mean((Q - m_Q) ** 2)
    v_IQ = mean((I - m_I) * (Q - m_Q))
    D_bar = v_IQ / v_I
    C_bar = sqrt(v_Q / v_I - D_bar**2)
    d_ampImb = sqrt(C_bar**2 + D_bar**2) - 1
    phi = arctan(D_bar / C_bar)
    I = I - m_I
    Q = ((Q - m_Q) / (1 + d_ampImb) - I * sin(phi)) / cos(phi)

    #%
    y = I + 1j * Q

    return y, phi
