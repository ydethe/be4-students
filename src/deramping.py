from multiprocessing import Pool
from re import M
from typing import Any
import os

from nptyping import NDArray
import numpy as np
from numpy.fft import fft, ifft, fftshift, fftfreq
from numpy import real, imag, log10
from scipy.signal import get_window
from matplotlib import pyplot as plt
from blocksim.dsp.DSPFilter import ArbitraryDSPFilter
from blocksim.graphics import plotBode

from readdata import readdata_radar2400AD2, iq_imbalance_comp
from be import Ts, c


class Signal(object):
    """Stores the data for one channel read from a file

    After init, this class has the following attributes:

    * d_mix : the (L time-samples * M sweeps) matrix of samples, with IQ balance if activated
    * num_sweeps : the number of sweeps (M)
    * num_samples_per_pri : the number of samples per sweep (L)
    * PRI (s) : the Pulse Repetition Interval (Tr)
    * LI (s) : the length of the pulse (here, =Tr)
    * B (Hz) : the instantaneous bandwidth of the pulse (B)
    * Fc (Hz) : the central frequency
    * chn : the index of the channel
    * Va : Ambiguous Velocity (m/s)
    * Ra : Ambiguous Range (m)

    Args:
        filename: The file where the sample sare written
        chn: Number of the channel. Can be 1 or 2
        fIQcomp: Flag to execute IQ balance

    """

    def __init__(self, filename: str, chn: int, fIQcomp: bool = True):
        date, wf, Fc, B, Tr, L, M, s_mix_1, s_mix_2 = readdata_radar2400AD2(filename)
        if chn == 1:
            s_mix = s_mix_1
        elif chn == 2:
            s_mix = s_mix_2
        else:
            raise ValueError(chn)

        ## IQ-imbalance compensation
        if fIQcomp:
            s_mix, _ = iq_imbalance_comp(s_mix)

        ## Reshape the mixed signal in "datacube"
        self.d_mix = s_mix.reshape((L, M), order="F")  # L time-samples * M sweeps
        self.num_sweeps = M
        self.num_samples_per_pri = L
        self.PRI = Tr
        self.LI = Tr
        self.B = B
        self.Fc = Fc
        self.chn = chn
        self.Va = c / (2 * Fc) / Tr  # (m/s) ambiguous velocity
        self.Ra = c * Tr / 2  # (m) ambiguous range

        print(76 * "-")
        print("Date: %s" % date)
        print("Waveform: %s" % wf)
        print("Carrier frequency:\t%.2f (GHz)" % (Fc * 1e-9))
        print("Bandwidwth:\t\t%.2f (GHz)" % (B * 1e-9))  # TODO: ajust print if not FMCW
        print("PRI:\t\t\t%.2f (ms)" % (Tr * 1e3))
        print("Samples per PRI:\t%d (-)" % L)
        print("Number of pulses:\t%d (-)" % M)
        print("Maximum range:\t\t%.2f (m)" % (c / 2 / B * L / 2))
        print("Ambiguous velocity:\t%.2f (m/s)" % self.Va)
        print("Ambiguous range:\t%.2f (km)" % (self.Ra * 1e-3))
        print(76 * "-")

    @property
    def range_resolution(self):
        """Range resolution of the waveform (m)"""
        dR = c / (2 * self.B)
        return dR

    def visu_iq_data(self):
        """Plots the IQ"""
        ## IQ data
        fig = plt.figure(1)
        # --
        axe = fig.add_subplot(1, 1, 1)
        axe.plot(real(self.d_mix[:1500, 0]), label=f"$I_{self.chn}$")
        axe.plot(imag(self.d_mix[:1500, 0]), label=f"$Q_{self.chn}$")
        axe.set_xlabel("sample (-)")
        axe.legend()
        axe.set_title(f"RX {self.chn}")
        axe.grid(True)

        return fig

    @staticmethod
    def dataVariance(data: NDArray[Any, Any]) -> float:
        """Returns the variance of the modulus of data"""
        s = np.var(np.abs(data))
        return s

    def rangeTransform(self, L_zp: int, win: str = "ones") -> NDArray[Any, Any]:
        """Performs the range transform of the signal

        Args:
            L_zp: Number of zeropadded range-gate
            win: Name of a window

        Returns:
            The (L time-samples * M sweeps) range-time matrix

        """
        w0 = get_window(win, self.num_samples_per_pri)
        win = np.empty((self.num_samples_per_pri, self.num_sweeps))
        for m in range(self.num_sweeps):
            win[:, m] = w0

        data = fftshift(ifft(win * self.d_mix, n=L_zp, axis=0), axes=0)

        return data

    def visu_range_time(self, range_data, Pmin: float = None, Pmax: float = None):
        """Plots the range-time map

        Args:
            range_data: The range-time matrix to plot
            Pmin (lin): Min power to clamp to
                If None, will be Pmax/1e3
            Pmax (lin): Max power to clamp to
                If None, Pmax in dB will be the multiple of 10 dB of twice the variance computed by Signal.dataVariance

        """
        if Pmax is None:
            Pmax = self.dataVariance(range_data) * 2
            pw = int(np.ceil(10 * log10(Pmax)) / 10) * 10
            Pmax = 10 ** (pw / 10)
            print(f"Automatic Pmax: {pw:.2f} dB")

        if Pmin is None:
            Pmin = Pmax / 1e3

        L_zp, M = range_data.shape
        rg_zp = (
            np.arange(0, 1, 1 / L_zp) - 0.5
        ) * self.num_samples_per_pri  # (-) zeropadded range-gate
        range_zp = rg_zp * self.range_resolution  # (m) range

        amp = np.clip(np.abs(range_data) ** 2, Pmin, Pmax)
        amp_db = 10 * log10(amp)
        Pmin_db = 10 * log10(Pmin)
        Pmax_db = 10 * log10(Pmax)

        X, Y = np.meshgrid(self.PRI * np.arange(M), range_zp)

        ## Display range-time intensity plot

        fig = plt.figure()
        # --
        axe = fig.add_subplot(1, 1, 1)
        im = axe.contourf(X, Y, amp_db, levels=np.arange(Pmin_db, Pmax_db + 2, 2))
        axe.set_xlabel("Time (s)")
        axe.set_ylabel("Range (m)")
        axe.set_title(f"RX {self.chn}")
        axe.set_ylim([0, range_zp[-1]])
        axe.grid(True)
        # --
        fig.suptitle("Range-time intensity plot")
        cbar = fig.colorbar(im)
        cbar.set_label("Power (dB)")

        return fig

    def MTIFilter(self, range_data: NDArray[Any, Any]) -> NDArray[Any, Any]:
        """Applies the MTI filter

        Args:
            range_data: The range-time matrix to process

        Returns:
            The processed data

        """
        range_data_mti = range_data[:, 1:] - range_data[:, :-1]
        return range_data_mti

    def range_doppler_map(
        self,
        range_data: NDArray[Any, Any],
        pulse0: int,
        npulse: int,
        nfft: int,
        win: str = "ones",
    ) -> NDArray[Any, Any]:
        """Performs the doppler transform of the signal

        Args:
            range_data: The range-time matrix to analyse
            pulse0: Index of the first pulse in the analysis
            npulse: Number pulses in the analysis
            nfft: Number of Doppler gates in the analysis
            win: Name of a window

        Returns:
            The (L time-samples * nfft doppler-samples) range-doppler matrix

            The time in secondes where the map was computed

        """
        dat = range_data[:, pulse0 : pulse0 + npulse]
        L_zp, nx = dat.shape

        w0 = get_window(win, nx)
        win = np.ones((L_zp, nx))
        for k in range(L_zp, self.num_samples_per_pri):
            win[k, :] = w0

        rd_map = fftshift(1 / nfft * fft(dat * win, n=nfft, axis=1), axes=1)
        # rd_map = 1 / nfft * fft(dat * win, n=nfft, axis=1)

        return rd_map, (pulse0 + npulse // 2) * self.PRI

    def visu_range_doppler(
        self,
        rd_map: NDArray[Any, Any],
        tmap: float,
        Pmin: float = None,
        Pmax: float = None,
    ):
        """Plots the range-doppler map

        Args:
            rd_map: The range-doppler matrix to plot
            tmap (s): The time where the map was computed (to set a title to the plot)
            Pmin (lin): Min power to clamp to
                If None, will be Pmax/1e3
            Pmax (lin): Max power to clamp to
                If None, Pmax in dB will be the multiple of 10 dB of 3 times the variance computed by Signal.dataVariance

        """
        L_zp, nx = rd_map.shape
        rg_zp = (
            np.arange(L_zp) / L_zp - 0.5
        ) * self.num_samples_per_pri  # (-) zeropadded range-gate
        range_zp = rg_zp * self.range_resolution  # (m) range

        if Pmax is None:
            Pmax = self.dataVariance(rd_map[L_zp // 2 :, :]) * 3
            pw = int(np.ceil(10 * log10(Pmax)) / 10) * 10
            Pmax = 10 ** (pw / 10)
            print(f"Automatic Pmax: {pw:.2f} dB")

        if Pmin is None:
            Pmin = Pmax / 1e3

        amp = np.clip(np.abs(rd_map) ** 2, Pmin, Pmax)
        amp_db = 10 * log10(amp)
        Pmin_db = 10 * log10(Pmin)
        Pmax_db = 10 * log10(Pmax)

        vr = (np.arange(nx) / nx - 0.5) * self.Va * 2

        fig = plt.figure()
        fig.suptitle("Range-Doppler map")
        axe = fig.add_subplot(1, 1, 1)
        axe.set_xlabel("Doppler (m/s)")
        axe.set_ylabel("Range (m)")
        axe.grid(True)

        X, Y = np.meshgrid(vr, range_zp)
        im = axe.contourf(X, Y, amp_db, levels=np.arange(Pmin_db, Pmax_db + 2, 2))
        axe.set_ylim([0, range_zp[-1]])
        axe.set_title(f"t = {tmap:.3f} s")
        cbar = fig.colorbar(im, ax=axe)
        cbar.set_label("Power (dB)")

        return fig, axe, im


def question_15():
    sig = Signal(filename="data/2017-07-27-17-07-46.mat", fIQcomp=True, chn=1)
    range_map = sig.rangeTransform(L_zp=sig.num_samples_per_pri * 8, win="hamming")

    sig.visu_range_time(range_map)


def question_16():
    # # No target, corridor with plants on ground in range-gate ca. 20-30 with hidden fan creating a windy environment
    # sig = Signal(filename="data/2017-07-27-16-57-05.mat", fIQcomp=True, chn=1)

    # # No target, corridor with plants on ground in range-gate ca. 20-30 with hidden fan off
    # sig = Signal(filename="data/2017-07-27-17-01-31.mat", fIQcomp=True, chn=1)

    # RC car receding-closing in corridor with plants on ground in range-gate ca. 20-30 with hidden fan creating a windy environment
    sig = Signal(filename="data/2017-07-27-17-07-46.mat", fIQcomp=True, chn=1)

    range_map = sig.rangeTransform(L_zp=sig.num_samples_per_pri * 8, win="hamming")
    range_map_mti = sig.MTIFilter(range_map)

    sig.visu_range_time(range_map_mti, Pmax=1e-2)


def question_17():
    f = ArbitraryDSPFilter(name="MTI", samplingPeriod=Ts, num=np.array([1, -1]))

    fig = plt.figure()
    gs = fig.add_gridspec(2, 1)
    plotBode(f, spec_amp=gs[0, 0], spec_pha=gs[1, 0])


def question_18():
    sig = Signal(filename="data/2017-07-27-17-07-46.mat", fIQcomp=True, chn=1)
    range_map = sig.rangeTransform(L_zp=sig.num_samples_per_pri * 8, win="hamming")
    range_map_mti = sig.MTIFilter(range_map)

    npulse = 32
    rd_map, tmap = sig.range_doppler_map(
        range_map,
        pulse0=0,
        npulse=npulse,
        nfft=npulse * 4,
        win="hamming",
    )
    fig, axe, im = sig.visu_range_doppler(rd_map, tmap=tmap, Pmax=1e-4)

    npulse = 32
    rd_map, tmap = sig.range_doppler_map(
        range_map_mti,
        pulse0=0,
        npulse=npulse,
        nfft=npulse * 4,
        win="hamming",
    )
    fig, axe, im = sig.visu_range_doppler(rd_map, tmap=tmap, Pmax=1e-4)


def process(d):
    pulse0 = d["pulse0"]
    sig = d["sig"]
    range_map_mti = d["range_map_mti"]

    print(f"Processing pulse {pulse0:06}")
    rd_map, tmap = sig.range_doppler_map(
        range_map_mti, pulse0=pulse0, npulse=32, nfft=128, win="hamming"
    )
    fig, axe, im = sig.visu_range_doppler(rd_map, tmap=tmap, Pmax=1e-4)
    fig.savefig(f"tmp/fig{pulse0:06}")
    fig.clf()
    plt.close()
    axe.cla()


def question_19():
    sig = Signal(filename="data/2017-07-27-17-07-46.mat", fIQcomp=True, chn=1)
    range_map = sig.rangeTransform(L_zp=sig.num_samples_per_pri * 8, win="hamming")
    range_map_mti = sig.MTIFilter(range_map)
    os.mkdir('tmp')

    # start 50 parallel worker processes
    with Pool(processes=50) as pool:
        pool.map(
            process,
            [
                {"pulse0": p, "sig": sig, "range_map_mti": range_map_mti}
                for p in range(0, sig.num_sweeps, 8)
            ],
        )

    fps = int(1 / (8 * sig.PRI))

    print(
        f"ffmpeg -framerate {fps} -pattern_type glob -i 'tmp/*.png' -c:v libx264 -pix_fmt yuv420p figures/out_mti.mp4"
    )


def test_range_doppler(t):
    sig = Signal(filename="data/2017-07-27-17-07-46.mat", fIQcomp=True, chn=1)
    range_map = sig.rangeTransform(L_zp=sig.num_samples_per_pri * 8, win="hamming")
    range_map_mti = sig.MTIFilter(range_map)

    npulse = 32
    rd_map, tmap = sig.range_doppler_map(
        range_map_mti,
        pulse0=int(t / sig.PRI) - npulse // 2,
        npulse=npulse,
        nfft=npulse * 4,
        win="hamming",
    )
    sig.visu_range_doppler(rd_map, tmap=tmap, Pmax=1e-4)


if __name__ == "__main__":
    question_15()
    question_16()
    question_17()
    question_18()
    # question_19()

    # # Unit test : at t = 15 s, we expect, according to the range_map_mti visualisation:
    # # * d = 4.873 m
    # * v = 2.757 m/s (careful of the ambiguity !)
    # test_range_doppler(15)

    # # Unit test : at t = 19.343 s, we expect, according to the range_map_mti visualisation:
    # # * d = 1.726 m
    # # * v = -2.003 m/s (careful of the ambiguity !)
    # test_range_doppler(19.343)

    # test_range_doppler(15)

    plt.show()
